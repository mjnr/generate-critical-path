const express = require('express'),
			bodyParser = require('body-parser'),
			morgan = require('morgan');

const app = express(),
			port = process.env.PORT || 8080;

app.use(morgan('short'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const router = express.Router();

const generateCriticalPath = require('./generateCriticalPath');

router.post('/criticalCss/', (req, res) => {
	const { customCriticalName, host, paths, css, penthouseOptions } = req.body;

	generateCriticalPath({ customCriticalName, paths, host, css, penthouseOptions })
		.then(criticalResponse => res.json(criticalResponse))
		.catch(err => res.status(err.statusCode || 500).json(err));
});

app.use('/api/v1', router);

const onAppListen = () => {
	console.log(`Server listening on ${port}!`);
};

app.listen(port, onAppListen);