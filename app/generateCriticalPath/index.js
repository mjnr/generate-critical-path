const penthouse = require('penthouse'),
			request = require('request'),
			md5 = require('md5'),
			fs = require('fs'),
			chalk = require('chalk'),
			logger = console.log;

const createCriticalFileName = (host) => `${md5(host)}.critical.css`;

const getBaseCssContent = (cssUrl) => {
	logger(chalk.gray(`\nGet main css file on ${cssUrl}`));

	return new Promise((resolve, reject) => {
		request.get(cssUrl, (err, res, baseCssContent) => {
			if (!err && res.statusCode === 200) {
				logger(chalk.green('Main css content is done!\n'));
				resolve(baseCssContent);
			} else {
				logger(chalk.red('Error on get main css content!\n'));
				reject(err);
			}
		})
	});
};

const startPenthouse = (criticalResponses, cssString, paths, criticalConfig, resolve, reject) => {
	if (paths.length > 0) {
		const { host, penthouseOptions = {}, customCriticalName = createCriticalFileName(host) } = criticalConfig,
					path = paths.shift();

		const url = `${host}${path}`;

		penthouse({
			url,
			cssString,
			...penthouseOptions
		})
		.then(criticalCss => {
			let fileName = customCriticalName;
			logger(chalk.green(`Critical css successfully generated based on route ${url}`));

			newCriticalsRes = [
				...criticalResponses,
				{
					url,
					fileName,
					criticalCss
				}
			];

			criticalResponses = newCriticalsRes;

			startPenthouse(criticalResponses, cssString, paths, criticalConfig, resolve, reject);
		})
		.catch(err => {
			let error = 'Error on penthouse critical generation';
			logger(chalk.red(error), err);
			reject(err);
		})
	} else {
		resolve(criticalResponses);
		criticalResponses = [];
		console.log('Done!');
	}
};

const generateCritical = (criticalConfig) => {
	let criticalResponses = [];

	logger(chalk.gray(`\nGenerating critical path ...`));

	return new Promise((resolve, reject) => {
		if (!criticalConfig) {
			let error = `criticalConfig object is not defined`;
			logger(chalk.red(error));
			reject(error);
		}

		let { paths, css } = criticalConfig;

		if (!criticalConfig.host || !criticalConfig.css) {
			let error = 'host or main css path is not defined!';
			logger(chalk.red(error));
			reject(error);
		}

		getBaseCssContent(css)
			.then(cssString => {
				startPenthouse(criticalResponses, cssString, paths, criticalConfig, resolve, reject);
			})
	});
};

module.exports = generateCritical;